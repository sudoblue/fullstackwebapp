import decode from "jwt-decode";

export const isAuthenticated = () => {
  const token = extractTokenData(localStorage.getItem("tr_at"));
  if (token !== null) {
    return { authed: true, verified: token.verified };
  }
  return { authed: false, verified: false };
};

export const extractTokenData = token => {
  if (!isTokenExpired(token)) {
    const token = localStorage.getItem("tr_at");
    const decoded = decode(token);
    return decoded;
  } else {
    return null;
  }
};

const isTokenExpired = token => {
  try {
    const decoded = decode(token);
    if (decoded.exp < Date.now() / 1000) {
      return true;
    } else return false;
  } catch (err) {
    return true;
  }
};
