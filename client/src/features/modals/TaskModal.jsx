import React, { Component } from "react";
import Modal from "./Wrapper";

export default class TaskModal extends Component {
  render() {
    const { title, closeModal } = this.props;
    return (
      <div>
        <Modal closeModal={closeModal} title={title}>
          Task
        </Modal>
      </div>
    );
  }
}
