import React from "react";
import TaskModal from "./TaskModal";
import ProjectModal from "./ProjectModal";

const lookup = {
  task: {
    component: TaskModal,
    title: "Add Task"
  },
  user: {
    component: TaskModal,
    title: "Add User"
  },
  project: {
    component: ProjectModal,
    title: "Add Project"
  }
};

const ModalManager = ({ modal, closeModal }) => {
  if (!modal) return null;
  const C = lookup[modal]["component"];
  const title = lookup[modal]["title"];
  return <C closeModal={closeModal} title={title} />;
};

export default ModalManager;
