import React, { Component } from "react";
import styled from "styled-components";

const ModalOverlay = styled.div`
  display: flex;
  justify-content: center;
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.3);
  height: 100vh;
  width: 100vw;
  z-index: 9999;
  opacity: 1;
  animation: show 0.2s ease;
  overflow-x: hidden;
  overflow-y: auto;
  user-select: none;
  border: none !important;
`;

const ModalCard = styled.div`
  margin-top: 10%;
  margin-bottom: 10%;
  min-height: 200px;
  width: 500px;
  background-color: ${props => props.theme.grey};
  display: grid;
  grid-template-rows: 50px 1fr 50px;
  grid-template-areas: "[header] [body] [buttons]";
`;

const ModalHeader = styled.div`
  grid-area: [header];
  color: black;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 22px;
`;

const ModalBody = styled.div`
  grid-area: [body];
  margin: 20px;
  color: black;
`;

class Modal extends Component {
  constructor(props) {
    super(props);

    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  componentDidMount() {
    window.addEventListener("keyup", this.handleKeyUp, false);
  }

  componentWillUnmount() {
    window.removeEventListener("keyup", this.handleKeyUp, false);
  }

  handleCloseModal(e) {
    if (e.target === e.currentTarget) this.props.closeModal();
  }

  handleKeyUp(e) {
    const { closeModal } = this.props;
    const keys = {
      27: () => {
        e.preventDefault();
        closeModal();
        window.removeEventListener("keyup", this.handleKeyUp, false);
      }
    };

    if (keys[e.keyCode]) {
      keys[e.keyCode]();
    }
  }

  render() {
    const { children, title = "" } = this.props;
    return (
      <ModalOverlay onClick={this.handleCloseModal}>
        <ModalCard>
          <ModalHeader>{title}</ModalHeader>
          <ModalBody>{children}</ModalBody>
        </ModalCard>
      </ModalOverlay>
    );
  }
}

export default Modal;
