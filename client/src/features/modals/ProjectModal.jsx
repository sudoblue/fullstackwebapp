import React, { Component } from "react";
import Modal from "./Wrapper";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const ADD_PROJECT_MUTATION = gql`
  mutation userlogin($username: String!, $password: String!) {
    userLogin(username: $username, password: $password)
  }
`;

export default class ProjectModal extends Component {
  state = {
    projectname: ""
  };

  handleAddProject = () => alert("shit");

  render() {
    const { title, closeModal } = this.props;
    return (
      <Modal closeModal={closeModal} title={title}>
        <Mutation mutation={ADD_PROJECT_MUTATION}>
          {(data, error, loading) => (
            <form>
              <label htmlFor="">Project Name</label>
              <input type="text" name="projectname" />
              <button onClick={() => this.handleAddProject()}>Create Project</button>
            </form>
          )}
        </Mutation>
      </Modal>
    );
  }
}
