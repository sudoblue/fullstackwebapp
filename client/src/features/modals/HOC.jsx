import React from "react";
import Conductor from "./Conductor";

const withModal = Component => {
  return class extends React.Component {
    state = {
      modal: null
    };

    showModal = modal => this.setState({ modal });
    closeModal = () => this.setState({ modal: null });

    render() {
      return (
        <React.Fragment>
          <Conductor modal={this.state.modal} closeModal={this.closeModal} />
          <Component showModal={this.showModal} />
        </React.Fragment>
      );
    }
  };
};

export default withModal;
