import React from "react";
import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const LoginCard = styled.div`
  align-self: center;
  width: 500px;
  background-color: ${props => props.theme.primary};
`;

export const ButtonContent = styled.div``;
export const Error = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  margin-top: 10px;
  width: 100%;
`;

export const ErrorMessage = styled.div`
  background-color: ${props => props.theme.warning};
  padding: 10px 30px;
`;

export const Title = styled.div`
  display: flex;
  color: white;
  padding: 20px;
  font-size: 30px;
  font-weight: 300;
  text-transform: uppercase;
`;

export const FieldsWrapper = styled.div`
  margin-bottom: 50px;
`;

export const RegisterMain = styled.div``;
export const LoginFields = styled.div`
  display: flex;
  flex-direction: column;
  label {
    padding: 10px 0px;
    padding-left: 45px;
  }
`;

export const ButtonGroup = styled.div`
  display: flex;
  height: 50px;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.theme.success};
  &:hover {
    background-color: ${props => props.theme.darksuccess};
    cursor: pointer;
  }
`;

const Field = styled.input`
  align-self: center;
`;

export const InputField = ({ name, value, text, handleTextChange, type }) => {
  return (
    <LoginFields>
      <label htmlFor={name}>{text}</label>
      <Field name={name} type={type} autoCorrect="off" autoComplete="off" value={value} onChange={handleTextChange} />
    </LoginFields>
  );
};
