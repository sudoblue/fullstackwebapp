import React from "react";
import { Transition } from "react-transition-group";

const duration = 200;

const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 0
};

const transitionStyles = {
  entering: { opacity: 0 },
  entered: { opacity: 1 }
};

const Fade = ({ in: inProp, message }) => (
  <Transition in={inProp} timeout={duration}>
    {state => {
      console.log(state);
      return (
        <div
          style={{
            ...defaultStyle,
            ...transitionStyles[state]
          }}
        >
          {message}
        </div>
      );
    }}
  </Transition>
);

export default Fade;
