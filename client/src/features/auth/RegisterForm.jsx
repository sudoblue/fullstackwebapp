import React, { Component } from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Redirect } from "react-router-dom";
import {
  Wrapper,
  LoginCard,
  Title,
  FieldsWrapper,
  ButtonGroup,
  InputField,
  ButtonContent,
  Error,
  ErrorMessage,
  RegisterMain
} from "./common";

const REGISTER_MUTATION = gql`
  mutation createAccount(
    $lastName: String!
    $firstName: String!
    $email: String!
    $password: String!
    $companyName: String
  ) {
    createAccount(
      lastName: $lastName
      firstName: $firstName
      email: $email
      password: $password
      companyName: $companyName
    ) {
      success
      message
    }
  }
`;

export default class RegisterForm extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    companyName: "",
    password: "",
    confirmPassword: "",
    success: false,
    submitting: false,
    submitError: "",
    view: "Owner"
  };

  handleSubmit = createAccount => {
    this.setState({ submitting: true, submitError: "" });
    const variables = { ...this.state };
    if (variables.password === variables.confirmPassword) {
      createAccount({ variables });
    }
  };

  handleTextChange = ({ target: { name, value } }) => this.setState({ [name]: value, submitError: "" });

  render() {
    const {
      lastName,
      firstName,
      email,
      companyName,
      password,
      confirmPassword,
      submitError,
      view,
      success
    } = this.state;
    return (
      <Mutation
        mutation={REGISTER_MUTATION}
        onError={err => this.setState({ submitting: false })}
        onCompleted={({ createAccount }) => {
          if (!createAccount.success) {
            this.setState({ submitError: createAccount["message"] });
          } else {
            console.log(createAccount);
            this.setState({ success: true });
          }
        }}
      >
        {(registerAccount, { data, error, loading }) => {
          if (success) {
            return <Redirect to="register/success" />;
          }
          return (
            <Wrapper>
              <RegisterMain>
                <Title>Are you an Owner or an Employee?</Title>
                <button onClick={() => this.setState({ view: "Owner" })}>
                  <i className="fa fa-user" />
                  Owner
                </button>
                <button onClick={() => this.setState({ view: "Employee" })}>
                  <i className="fa fa-users" />
                  Employee
                </button>
              </RegisterMain>

              <LoginCard>
                <FieldsWrapper>
                  {view === "Owner" && (
                    <InputField
                      text="Company Name"
                      type="text"
                      name="companyName"
                      value={companyName}
                      handleTextChange={this.handleTextChange}
                    />
                  )}

                  <InputField
                    text="First Name"
                    type="text"
                    name="firstName"
                    value={firstName}
                    handleTextChange={this.handleTextChange}
                  />
                  <InputField
                    text="Last Name"
                    type="text"
                    name="lastName"
                    value={lastName}
                    handleTextChange={this.handleTextChange}
                  />

                  <InputField
                    text="Email"
                    type="email"
                    name="email"
                    value={email}
                    handleTextChange={this.handleTextChange}
                  />
                  <InputField
                    text="Password"
                    type="password"
                    name="password"
                    value={password}
                    handleTextChange={this.handleTextChange}
                  />
                  <InputField
                    text="Confirm Password"
                    type="password"
                    name="confirmPassword"
                    value={confirmPassword}
                    handleTextChange={this.handleTextChange}
                  />
                  <Error>{submitError && <ErrorMessage>{submitError}</ErrorMessage>}</Error>
                </FieldsWrapper>

                <ButtonGroup
                  onClick={() => this.handleSubmit(registerAccount)}
                  handleTextChange={this.handleTextChange}
                >
                  {loading ? <ButtonContent>Loading</ButtonContent> : <ButtonContent>CREATE ACCOUNT</ButtonContent>}
                </ButtonGroup>
              </LoginCard>
            </Wrapper>
          );
        }}
      </Mutation>
    );
  }
}
