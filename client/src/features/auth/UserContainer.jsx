import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { AppFooter, AppHeader, AppLayout, AppMain } from "../../layout/App";
import Footer from "../../layout/footer/Footer";
import AnonHeader from "../../layout/header/AnonHeader";
import Login from "./LoginForm";
import Register from "./RegisterForm";
import AccountNotVerified from "./AccountNotVerified";

const UserContainer = props => (
  <AppLayout>
    <AppHeader>
      <AnonHeader push={props.history.push} />
    </AppHeader>
    <AppMain>
      <Switch>
        <Redirect exact from="/user" to="/user/login" />
        <Route exact path="/user/login/verify" component={AccountNotVerified} />
        <Route path="/user/login" component={Login} />

        <Route
          exact
          path="/user/register/success"
          render={() => <div>Success! Please check your email to verify your account.</div>}
        />
        <Route path="/user/register" component={Register} />
        <Route path="/user/forgot-password" render={() => "forgot password"} />
        <Route path="/user/confirm" render={() => "confirm password reset"} />
        <Redirect from="*" to="/user" />
      </Switch>
    </AppMain>
    <AppFooter>
      <Footer />
    </AppFooter>
  </AppLayout>
);

export default UserContainer;
