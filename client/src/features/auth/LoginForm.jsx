import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Wrapper, LoginCard, Title, FieldsWrapper, ButtonGroup, InputField } from "./common";
import Fade from "./ToastError";

const LOGIN_MUTATION = gql`
  mutation userlogin($username: String!, $password: String!) {
    userLogin(username: $username, password: $password) {
      success
      message
      token
    }
  }
`;

class Login extends React.Component {
  state = {
    username: "",
    password: "",
    hasError: false,
    loginError: "",
    submitting: false
  };

  handleSubmit = login => {
    this.setState({ submitting: true });
    const variables = { ...this.state };
    login({ variables });
  };

  handleTextChange = ({ target: { name, value } }) => this.setState({ [name]: value, loginError: "", hasError: false });

  handleOnError = err => {
    console.log(err);
    this.setState({ loginError: "An error has occurred!" });
  };

  handleOnCompleted = userLogin => {
    const { history } = this.props;
    if (userLogin.success) {
      localStorage.setItem("tr_at", userLogin.token);
      if (!userLogin.verified) {
        history.push("/user/login/verify");
      } else {
        window.location.replace("/home");
      }
    } else {
      this.setState({ loginError: userLogin.message, password: "", hasError: true });
    }
  };
  render() {
    const { username, password, loginError, hasError } = this.state;
    return (
      <Mutation
        mutation={LOGIN_MUTATION}
        onError={err => this.handleOnError(err)}
        onCompleted={({ userLogin }) => this.handleOnCompleted(userLogin)}
      >
        {userLogin => (
          <Wrapper>
            <Fade in={hasError} message={loginError} />
            <LoginCard>
              <div className="left-side">
                <Title>Login</Title>
              </div>
              <div className="right-side">
                <FieldsWrapper>
                  <InputField
                    text="Email Address"
                    type="email"
                    name="username"
                    value={username}
                    handleTextChange={this.handleTextChange}
                  />

                  <InputField
                    text="Password"
                    type="password"
                    name="password"
                    value={password}
                    handleTextChange={this.handleTextChange}
                  />
                </FieldsWrapper>
                <ButtonGroup onClick={() => this.handleSubmit(userLogin)}>LOGIN</ButtonGroup>
              </div>
            </LoginCard>
          </Wrapper>
        )}
      </Mutation>
    );
  }
}

export default Login;
