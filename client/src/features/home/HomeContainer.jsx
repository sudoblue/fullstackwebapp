import React from "react";
import styled from "styled-components";
import withModal from "../modals/HOC";
import MyTasks from "./tasks/MyTasks";
import AllTasks from "./tasks/AllTasks";

const HomeWrapper = styled.div`
  height: 100%;
  display: grid;
  grid-template-rows: 40px 1fr 40px;
  grid-row-gap: 10px;
  grid-template-areas:
    "buttonBar"
    "tasks"
    "time";

  i {
    transition: all 0.2s linear;
    margin: 0 5px;
    color: ${props => props.theme.primary};
    &:hover {
      cursor: pointer;
      color: ${props => props.theme.success};
    }
  }
`;

const ButtonToolbar = styled.div`
  grid-area: [buttonBar];
  display: flex;
  justify-content: flex-end;
`;
const TaskWrapper = styled.div`
  grid-area: [tasks];
`;

const MyTasksWrapper = styled.div``;

const RecentTasksWrapper = styled.div``;

const SectionTitle = styled.div`
  font-size: 24px;
  font-weight: 500;
`;

const SectionBody = styled.div`
  border-radius: 10px;
  background-color: ${props => props.theme.primary};
  padding: 10px;
  margin: 20px 0;
`;

const HomeContainer = ({ showModal }) => {
  return (
    <HomeWrapper>
      <ButtonToolbar>
        <i className="fa fa-2x fa-tasks" onClick={() => showModal("task")} />
        <i className="fa fa-2x fa-project-diagram" onClick={() => showModal("project")} />
        <i className="fa fa-2x fa-user-plus" onClick={() => showModal("user")} />
      </ButtonToolbar>
      <TaskWrapper>
        <MyTasksWrapper>
          <SectionTitle>My Tasks</SectionTitle>
          <SectionBody>
            <MyTasks />
          </SectionBody>
        </MyTasksWrapper>

        <RecentTasksWrapper>
          <SectionTitle>Recent Tasks</SectionTitle>
          <SectionBody>
            <AllTasks />
          </SectionBody>
        </RecentTasksWrapper>
      </TaskWrapper>
    </HomeWrapper>
  );
};

export default withModal(HomeContainer);
