import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: process.env.REACT_APP_GQL_ENDPOINT,
  fetchOptions: {
    credentials: "include"
  },
  request: async operation => {
    const token = await localStorage.getItem("tr_at");
    operation.setContext({
      headers: {
        authorization: token || ""
      }
    });
  }
});

export default client;
