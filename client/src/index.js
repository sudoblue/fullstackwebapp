import "reset-css";
import "./styles.css";
import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "react-apollo";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import App from "./layout/App";
import { ThemeProvider } from "styled-components";
import theme from "./theme";
import client from "./gqlClient";
import UserContainer from "./features/auth/UserContainer";
import { isAuthenticated } from "./jwt";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { authed, verified } = isAuthenticated();
  if (authed && verified) {
    return <Route {...rest} component={props => <Component {...props} />} />;
  }
  if (authed && !verified) {
    return <Redirect to="/user/login/verify" />;
  }

  return <Redirect to="/user/login" />;
};

const Application = (
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Switch>
          <Route path="/user" component={UserContainer} />
          <PrivateRoute path="/" component={App} />
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  </ApolloProvider>
);

ReactDOM.render(Application, document.getElementById("root"));
