export default {
  success: "mediumseagreen",
  primary: "#2c3e50",
  darkgrey: "#677077",
  secondary: "#34495e",
  darksuccess: "#308f59",
  warning: "#EC644B",
  alert: "#F4D03F",
  darkalert: "#e8bc0d",
  grey: "#E3E6E9"
};
