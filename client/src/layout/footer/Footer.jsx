import React from "react";

const Footer = () => (
  <div className="copyright">
    <img className="logo footer-icon" src="/logo.svg" alt="teams react logo" height="20px" />
    TeamsReact &copy;
  </div>
);

export default Footer;
