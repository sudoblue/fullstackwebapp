import React from "react";
import { HeaderWrapper, NavButtons, Button, HeaderLeft, TeamName } from "./styles";

const AnonHeader = ({ push, teamName = "TeamsReact" }) => (
  <HeaderWrapper>
    <HeaderLeft>
      <img className="logo" src="/logo.svg" alt="logo" height="50px" onClick={() => push("/home")} />
      <TeamName onClick={() => push("/home")}>{teamName}</TeamName>
    </HeaderLeft>
    <NavButtons>
      <Button register onClick={() => push("/user/register")}>
        Sign Up
      </Button>
      <Button onClick={() => push("/user/login")}>Login</Button>
    </NavButtons>
  </HeaderWrapper>
);

export default AnonHeader;
