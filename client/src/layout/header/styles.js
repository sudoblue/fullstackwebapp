import styled from "styled-components";

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
`;

export const NavButtons = styled.div``;
export const Button = styled.button`
  padding: 10px 20px;
  font-size: 14px;
  text-transform: uppercase;
  margin: 5px;
  color: white;
  font-weight: 500;
  background-color: ${props => props.theme.alert};

  &:hover {
    background-color: ${props => props.theme.darkalert};
  }
`;

export const HeaderLinks = styled.div`
  a {
    margin: 0 5px;
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
`;

export const HeaderLeft = styled.div`
  align-self: center;
  display: flex;
  align-items: center;
`;

export const TeamName = styled.div`
  font-weight: normal;
  font-size: 20px;
  padding-left: 20px;
  padding-right: 20px;
  &:hover {
    user-select: none;
    cursor: pointer;
  }
`;

export const HeaderRight = styled.div`
  &:hover {
    cursor: pointer;
    color: grey;
  }
`;
