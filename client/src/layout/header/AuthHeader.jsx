import React from "react";
import { NavLink } from "react-router-dom";
import { HeaderWrapper, HeaderLinks, HeaderLeft, TeamName, HeaderRight } from "./styles";

const AuthHeader = ({ push, teamName = "TeamsReact" }) => (
  <HeaderWrapper>
    <HeaderLeft>
      <img className="logo" src="/logo.svg" alt="logo" height="50px" onClick={() => push("/home")} />
      <TeamName onClick={() => push("/home")}>{teamName}</TeamName>

      <HeaderLinks>
        <NavLink to="/tasks">Tasks</NavLink>
        <NavLink to="/projects">Projects</NavLink>
        <NavLink to="/users">Users</NavLink>
      </HeaderLinks>
    </HeaderLeft>

    <HeaderRight
      onClick={() => {
        localStorage.removeItem("tr_at");
        window.location.replace("/user/login");
      }}
    >
      Logout
    </HeaderRight>
  </HeaderWrapper>
);

export default AuthHeader;
