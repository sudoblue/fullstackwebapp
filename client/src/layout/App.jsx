import React, { Component } from "react";
import AuthHeader from "./header/AuthHeader";
import Footer from "./footer/Footer";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "../features/home/HomeContainer";
import styled from "styled-components";

export const AppLayout = styled.div`
  min-height: 100vh;
  height: 100vh;
  color: ${props => props.theme.main};
  display: grid;
  grid-template-rows: 60px 1fr 40px;
  grid-template-areas:
    "header"
    "main"
    "footer";
  margin-bottom: -50px;
`;

export const AppHeader = styled.div`
  grid-area: header;
  padding: 0px 15%;
  background-color: var(--primary);
  position: sticky;
  top: 0;
`;

export const AppMain = styled.div`
  grid-area: main;
  padding: 20px 15%;
`;

export const AppFooter = styled.div`
  display: flex;
  align-items: center;
  padding: 10px 15%;
  grid-area: footer;
  background-color: var(--primary);
  bottom: 0;
`;

export default class Application extends Component {
  state = { modal: null };
  render() {
    const { pathname } = this.props.location;
    const activePage = pathname.split("/")[1];
    const { push } = this.props.history;
    return (
      <AppLayout>
        <AppHeader>
          <AuthHeader push={push} teamName="TEAM" activePage={activePage} />
        </AppHeader>
        <AppMain>
          <Switch>
            <Redirect exact from="/" to="/home" />
            <Route path="/home" component={Home} />
            <Route path="/tasks" render={() => "tasks"} />
            <Route path="/projects" render={() => "projects"} />
            <Route path="/users" render={() => "users"} />
          </Switch>
        </AppMain>
        <AppFooter>
          <Footer />
        </AppFooter>
      </AppLayout>
    );
  }
}
