/* Replace with your SQL commands */
create table if not exists projects 
(
    id serial PRIMARY KEY,
    name varchar(100),
    status project_type,
    created timestamp
)