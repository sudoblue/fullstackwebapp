/* Replace with your SQL commands */
CREATE TABLE
IF NOT EXISTS Users
(
    id SERIAL NOT NULL PRIMARY KEY,
    lastName varchar
(100) NOT NULL,
    FirstName varchar
(100) NOT NULL,
    hashedPassword varchar
(255) NOT NULL,
    email varchar
(100) NOT NULL,
    verified Boolean
)