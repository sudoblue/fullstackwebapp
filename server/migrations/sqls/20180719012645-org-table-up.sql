/* Replace with your SQL commands */

CREATE TABLE
IF NOT EXISTS organizations
(
    id SERIAL NOT NULL PRIMARY KEY,
    name varchar(100) NOT NULL
)