const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const helmet = require("helmet");
const { ApolloServer } = require("apollo-server-express");
const typeDefs = require("./graphql/schemas");
const resolvers = require("./graphql/resolvers");
const jwt = require("jsonwebtoken");
const app = express();

app.use(morgan("dev"));
app.use(cors("*"));
app.use(helmet());

const server = new ApolloServer({
  typeDefs,
  resolvers,
  tracing: false,
  context: ({ req }) => {
    let user = null;
    let token = req.headers.authorization || "";
    if (token !== "") {
      user = jwt.verify(token, process.env.JWT_SECRET);
    }
    return { user };
  }
});

server.applyMiddleware({ app });

module.exports = app;
