const userErrors = {
  emailExists: "Email already exists.",
  companyExists: "Company name not available."
};

const errorHandler = error => {
  if (userErrors[error]) {
    return userErrors[error];
  } else {
    return "Something went wrong.";
  }
};

module.exports = errorHandler;
