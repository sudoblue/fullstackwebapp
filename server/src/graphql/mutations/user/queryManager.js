const lookup = {
  findEmail: {
    text: "select * from users where email = $1"
  },
  createCompany: {
    text: "insert into organizations (name) values ($1) returning (id)"
  },
  createAccount: {
    text: `INSERT INTO users (lastname, firstname, hashedpassword, email, verified, owner, orgid, verifytoken) VALUES ($1,$2,$3,$4,$5, $6, $7, $8)`
  },
  findCompany: {
    text: "select * from organizations where name = $1"
  }
};

const queryManager = (queryName, queryValues) => {
  const query = lookup[queryName];
  query.values = queryValues;
  return query;
};

module.exports = queryManager;
