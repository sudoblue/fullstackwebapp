const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const db = require("../../../db");
const crypto = require("crypto");
const queryManager = require("./queryManager");
const parseError = require("parse-error");
const errorHandler = require("./errors");
const Mailer = require("../../mailer");

const userResponse = (success, message = null, token = null, verified = false) => {
  const response = {};
  response.success = success;
  if (message) response.message = message;
  if (token) response.token = token;
  response.verified = verified;
  return response;
};

const userLogin = async (root, { username, password }, ctx) => {
  try {
    const findEmailQuery = queryManager("findEmail", [username]);
    const userFound = await db.query(findEmailQuery);
    const user = userFound.rows[0];

    if (user && bcrypt.compareSync(password, user.hashedpassword)) {
      const token = jwt.sign(
        {
          userId: user.id,
          orgId: user.orgid,
          verified: user.verified
        },
        process.env.JWT_SECRET,
        {
          expiresIn: process.env.JWT_EXPIRES
        }
      );
      return userResponse(true, null, token, user.verified);
    } else {
      return userResponse(false, "invalid login credentials.");
    }
  } catch (error) {
    console.log(error);
    return userResponse(false, "error logging in.");
  }
};

const createAccount = (root, params, ctx) => {
  const { lastName, firstName, password, email, companyName = null } = params;
  let owner = false;
  if (companyName) owner = true;
  let verified = false;

  return (async () => {
    const client = await db.pool.connect();
    try {
      await client.query("BEGIN");

      const userSearch = await db.query(queryManager("findEmail", [email]));
      if (userSearch.rowCount === 1) throw new Error("emailExists");

      const companySearch = await db.query(queryManager("findCompany", [companyName]));
      if (companySearch.rowCount === 1) throw new Error("companyExists");

      const createCompany = await db.query(queryManager("createCompany", [companyName]));
      const companyId = createCompany.rows[0].id;
      const hashedPassword = await bcrypt.hash(password, 12);

      const tokenBuffer = crypto.randomBytes(20);
      const verifyToken = tokenBuffer.toString("hex");

      await db.query(
        queryManager("createAccount", [
          lastName,
          firstName,
          hashedPassword,
          email,
          verified,
          owner,
          companyId,
          verifyToken
        ])
      );

      await client.query("COMMIT");

      // Send email to new user
      const verifyOptions = {
        firstName,
        lastName,
        email,
        verifyToken
      };
      const mail = new Mailer();
      mail.verifyNewAccount(verifyOptions);
      return userResponse(true);
    } catch (e) {
      await client.query("ROLLBACK");
      throw e;
    } finally {
      client.release();
    }
  })().catch(e => {
    console.error(e);
    let tempMsg = parseError(e).message;
    const responseMsg = errorHandler(tempMsg);
    return userResponse(false, responseMsg);
  });
};

const resetPassword = async (root, args, ctx) => {
  const tokenBuffer = crypto.randomBytes(20);
  const resetToken = tokenBuffer.toString("hex");
  return resetToken;
};

const resendVerificationEmail = () => {};

const confirmPasswordReset = () => {};

module.exports = { userLogin, createAccount, resetPassword, confirmPasswordReset };
