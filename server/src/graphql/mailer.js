const nodemailer = require("nodemailer");

class Mailer {
  constructor() {
    this.transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "teamsreact@gmail.com",
        pass: "r3dmasB3"
      }
    });
  }

  verifyNewAccount({ firstName, lastName, verifyToken, email }) {
    const confirmAccountOptions = {
      from: '"Teams React" <teamsreact@gmail.com>',
      to: email,
      subject: "Welcome to TeamsReact!",
      html: `
      <div>
      <h1> Hi ${firstName} ${lastName}! </h1>
      <p>Welcome to teams react.</p>
      <span>
        http://localhost:3000/user/confirm?email=${email}&token=${verifyToken}
        </span>
      </div>
      `
    };

    this.transporter.sendMail(confirmAccountOptions, (error, info) => {
      if (error) console.error(error);
      console.log("mail info", info);
    });
  }
}

module.exports = Mailer;
