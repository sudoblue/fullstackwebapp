const { gql } = require("apollo-server");

const projectsSchema = gql`
  type Project {
    id: Int!
    name: String!
  }
`;

module.exports = projectsSchema;
