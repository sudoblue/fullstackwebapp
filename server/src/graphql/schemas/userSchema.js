const { gql } = require("apollo-server");

const userSchema = gql`
  type Query {
    loadUserData: userDataResponse
  }

  type userDataResponse {
    message: String!
    projects: [Project]
  }

  type GenericResponse {
    success: Boolean!
    message: String
    token: String
    verified: Boolean!
  }

  type Mutation {
    userLogin(username: String!, password: String!): GenericResponse
    resetPassword(test: String): String
    createAccount(
      companyName: String
      firstName: String
      lastName: String
      email: String
      password: String
    ): GenericResponse
  }
`;

module.exports = userSchema;
