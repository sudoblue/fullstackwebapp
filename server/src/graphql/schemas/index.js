const userDefs = require("./userSchema");
const projectDefs = require("./projects");
module.exports = [userDefs, projectDefs];
