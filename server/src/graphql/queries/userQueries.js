const db = require("../../db");

const loadHomeData = () => {
  return {
    message: "test",
    projects: [{ id: 1, name: "Project 1" }]
  };
};

module.exports = {
  loadHomeData
};
