const compose = require("./utils/compose");
const isAuthenticated = require("./utils/isAuthenticated");
const { createAccount, userLogin, resetPassword } = require("../mutations/user/user");
const { loadHomeData } = require("../queries/userQueries");

const userResolvers = {
  Query: {
    loadUserData: compose(isAuthenticated)(loadHomeData)
  },
  Mutation: {
    createAccount: (root, args, ctx) => createAccount(root, args, ctx),
    userLogin: (root, args, ctx) => userLogin(root, args, ctx),
    resetPassword: (root, args, ctx) => resetPassword(root, args, ctx)
  }
};

module.exports = userResolvers;
