const isAuthenticated = fn => (parent, args, context, info) => {
  if (context.user) {
    return fn(parent, args, context, info);
  }
  throw new Error("User is not authenticated");
};

module.exports = isAuthenticated;
